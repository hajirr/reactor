import React, { useState } from "react";
import { FiMenu } from "react-icons/fi";
import { MobileNavbar } from "./MobileNavbar";
import { DesktopNavbar } from "./DesktopNavbar";

export const Navbar = () => {
  const [navOpen, setnavOpen] = useState(false);
  const menuClicked = () => {
    navOpen ? setnavOpen(false) : setnavOpen(true);
  };
  return (
    <div
      className={
        navOpen
          ? "bg-red-100 h-auto w-screen sm:h-screen sm:w-72"
          : "bg-red-100 h-20 w-screen sm:h-screen sm:w-72"
      }
    >
      <FiMenu
        onClick={menuClicked}
        className="inline-block ml-2 my-5 text-4xl sm:hidden"
      />
      <div className={navOpen ? "w-screen bg-red-200 p-2 sm:hidden" : "hidden"}>
        <MobileNavbar />
      </div>
      <div className="hidden sm:block">
        <DesktopNavbar />
      </div>
    </div>
  );
};
