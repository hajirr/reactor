import React from 'react'
import { BiHomeCircle } from 'react-icons/bi'
import { BsPersonCircle } from 'react-icons/bs'
import { FaCrown } from 'react-icons/fa'

export const MobileNavbar = () => {
    return (
        <><div className="flex block mb-2">
            <BiHomeCircle className="my-auto mr-2 text-4xl" />
            <span className="my-auto font-bold text-1xl">DASHBOARD</span>
        </div><div className="flex block mb-2">
                <BsPersonCircle className="my-auto mr-2 text-4xl" />
                <span className="my-auto font-bold text-1xl">MY PROFILE</span>
            </div><div className="flex block mb-2">
                <FaCrown className="my-auto mr-2 text-4xl" />
                <span className="my-auto font-bold text-1xl">PREMIUM</span>
            </div><div className="flex block mb-2">
                <BiHomeCircle className="my-auto mr-2 text-4xl" />
                <span className="my-auto font-bold text-1xl">PRICE LIST</span>
            </div><div className="flex block mb-2">
                <BiHomeCircle className="my-auto mr-2 text-4xl" />
                <span className="my-auto font-bold text-1xl">SALDO</span>
            </div><div className="flex block mb-2">
                <BiHomeCircle className="my-auto mr-2 text-4xl" />
                <span className="my-auto font-bold text-1xl">HISTORY ORDER</span>
            </div><div className="flex block mb-2">
                <BiHomeCircle className="my-auto mr-2 text-4xl" />
                <span className="my-auto font-bold text-1xl">SUPPORT TICKET</span>
            </div><div className="flex block mb-2">
                <BiHomeCircle className="my-auto mr-2 text-4xl" />
                <span className="my-auto font-bold text-1xl">FORUM</span>
            </div><div className="flex block mb-2">
                <BiHomeCircle className="my-auto mr-2 text-4xl" />
                <span className="my-auto font-bold text-1xl">DAFTAR REFUND</span>
            </div><div className="flex block mb-2">
                <BiHomeCircle className="my-auto mr-2 text-4xl" />
                <span className="my-auto font-bold text-1xl">BANTUAN</span>
            </div></>
    )
}
