import React from 'react';
import { Body } from './components/Body/Body';
import { Navbar } from './components/Navbar/Navbar';

function App() {
  return (
    <>
    <div className="sm:flex">
    <Navbar /><Body />

    </div>
    
    </>
  );
}

export default App;
